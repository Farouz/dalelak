@extends('Site.layout.master')
@section('title')
    {{$category->name_ar}}
@stop
@section('content')
    <div class="clearfix"></div>
    <div class="tqniah-title-page Tqniah-title-font">
        <h1 class="wow animate pulse" data-wow-duration="1.5s">{{$category->name_ar}}</h1>
    </div>

    <section id="tqniah-content-coffees">
        <div class="container">
            <div class="tqniah-coffees-div">


                @foreach($category->Companies as $company)
                    @if($company->User->active == 1 )
                        <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                            <div class="tqniah-single-caffees">
                                <div class="tqniah-img-cafe">
                                    <img src="{{asset('assets/images/'.$company->avatar)}}" alt="{{$company->title}}"/>
                                    <span class="tqniah-discount wow animate fadeInLeft">
                                        @if(!$company->discount == null )
                                            15%
                                            <br/>
                                            خصم
                                        @else
                                            <small>                                        لا  تخفيض
</small>
                                        @endif
						</span>
                                </div>
                                <div class="tqniah-text-cafe">
                                    <div class="tqniah-subtext-cafe">
                                        <h4>{{$company->title}}</h4>
                                        <p>{{substr($company->mini_desc,0,50)}}</p>

                                        <small class="tqniah-hidden-di">
                                            <ul>
                                                <li class="dropdown">
                                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"
                                                       role="button"
                                                       aria-haspopup="true" aria-expanded="false">
                                                        <i class="fa fa-ellipsis-v" aria-hidden="true"></i>
                                                    </a>
                                                    <ul class="dropdown-menu dropdown-menu-right">
                                                        <li>
                                                            <a href="{{route('GET_SUB_DETAILS',$company->id)}}">التفاصيل</a>
                                                        </li>
                                                    </ul>
                                                </li>
                                            </ul>
                                        </small>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endif
                @endforeach


            </div>
        </div>
    </section>

@stop