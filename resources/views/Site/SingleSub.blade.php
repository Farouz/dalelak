@extends('Site.layout.master')
@section('title')
    {{$company->title}}
@stop
@section('content')
    <div class="tqnih-bg-body-home">
        <div class="tqniah-bg-color-home">
            <!--===============================
                HEADER
            ===================================-->

            <section id="tqniah-header">
                <div class="container">
                    <div class="col-lg-9 col-md-9 col-sm-9 col-xs-12">
                        <div class="tqniah-navbar">
                            <div class="nav-tqniah">
                                <nav>
                                    <button type="button" class="hidden-lg hidden-md hidden-sm">
                                        <span></span>
                                        <span></span>
                                        <span></span>
                                    </button>
                                </nav>
                            </div>
                        </div><!-- END NAV -->
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                        <div class="row" id="tqinah-counter">
                            <div class="tqniah-count">
                            </div>
                        </div>
                    </div><!-- END Visite -->
                    <div class="clearfix"></div>

                    <div class="tqniah-slide-datails">
                        <div id="owl-demo2" class="loop1 owl-carousel">
                            @foreach(explode(' ',$company->sliders) as $slide)
                                <div class="item"><img src="{{asset('assets/images/'.$slide)}}" alt="Owl Image"></div>
                            @endforeach
                        </div>
                    </div>
                </div><!-- END Container -->
                <div class="tqniah-part-bg"></div>
            </section><!-- END Header -->
        </div><!-- END BG color Body -->
    </div><!-- END BG Image Body -->
    <!--===============================
        Content
    ===================================-->

    <section id="tqniah-datail-content">
        <div class="container">
            <div class="tqniah-map ">
                <div class="tqniah-img-map hidden-xs  wow animate slideInLeft" data-wow-duration="3s">
                    <img src="{{asset('assets/images/'.$company->avatar)}}" alt="{{$company->title}}"/>
                </div>
                <div class="tqniah-map-site">
                    <div id="map"></div>
                </div>
            </div>
            <div class="tqniah-description-datails" style="background-color:rgba(0,0,0,.5);">
                <div class="top-desc-datails">
                    <div class="col-lg-9 col-md-9 col-sm-7 col-xs-12">
                        <h3>الأسم : {{$company->title}}</h3>
                        <h3>العنوان : {{$company->address}}</h3>
                        <h3>رقم التواصل : {{$company->phone}}</h3>
                    </div>

                </div>
                <p>
                    {{$company->main_desc}}                </p>
            </div>
            <div class="tqniah-menu-food" style="background-color:rgba(0,0,0,.5); ">
                <div class="tqniah-tab-content">
                    <h2><span>المنيو</span></h2>
                    <ul class="nav nav-tabs">
                        @php
                            $i=0;
                        $x=0;
                        @endphp
                        @foreach($company->Menus as $menu )

                            <li @if($i == 1) class="active" @endif><a data-toggle="tab"
                                                                      href="#tqniah-tabs-{{$menu->id}}"
                                                                      aria-expanded="false">{{$menu->title}}</a></li>
                            @php
                                $i++;
                            @endphp
                        @endforeach

                    </ul>
                    <div class="clearfix"></div>
                    <div class="tab-content">
                        @foreach($company->Menus as $menu)
                            <div id="tqniah-tabs-{{$menu->id}}" class="tab-pane fade active in  ">
                                <div class="tqniah-slide-menu">
                                    <div id="owl-demo3" class="loop2 owl-carousel">
                                        <div class="item">
                                            @foreach($menu->SubMenus as $subMenu)
                                                <div class="col-md-3 col-sm-4 col-xs-12">
                                                    <div class="wow animate fadeInUp" data-wow-duration="1.5s">
                                                        <div class="tqniah-info-type">
                                                            <img src="{{asset('assets/images/'.$subMenu->avatar)}}"
                                                                 alt="cat"/>
                                                            <h3></h3>
                                                            <p>{{$subMenu->title}}</p>
                                                            <p>{{$subMenu->salary}} ج.م</p>
                                                        </div>
                                                    </div>
                                                </div>
                                            @endforeach
                                        </div>
                                    </div>
                                </div>
                            </div>
                            @php
                                $x++;
                            @endphp
                        @endforeach
                    </div>
                </div>
            </div>

            <div class="tqniah-comments" style="background-color:rgba(0,0,0,.5);">

                <h2><span>التعليقات </span></h2>

                <div class="clearfix"></div>
                <div class="tqniah-form-show">
                    @foreach($company->Comments as $comment)
                        <div class="tqniah-single-comment wow animate slideInUp" data-wow-duration="1.5s">
                            <h5><span>{{substr($comment->name,0,2)}}</span></h5>
                            <div class="tqniah-content-comment">
                                <h3>{{$comment->name}}</h3>
                                <p>{{$comment->comment}}</p>
                            </div>
                        </div>
                    @endforeach


                    <div class="tqniah-comment-form">

                        <form action="{{route('SEND_COMMENT',$company->id)}}" method="post"
                              enctype="multipart/form-data">
                            {{csrf_field()}}

                            <input type="text" name="name" placeholder="الأسم" required>
                            <input type="email" name="email" placeholder="البريد الإلكتروني" required>
                            <textarea name="comment" placeholder="تعليقك هنا" required></textarea>
                            <input type="hidden" name="company_id" value="{{$company->id}}">
                            <input type="submit" name="submit" value="تعليق"/>
                        </form>
                    </div>

                </div>

            </div>

        </div>
    </section>

@stop
@section('scripts')
    <script>
        function initMap() {
            var uluru = {lat: {{$company->lat}}, lng: {{$company->lan}}};
            var map = new google.maps.Map(document.getElementById('map'), {
                zoom: 17,
                center: uluru
            });
            var marker = new google.maps.Marker({
                position: uluru,
                map: map
            });
        }
    </script>
    <script async defer
            src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBHqEE22ot1vzLTlusR3DD0mRX7e2HLYwE&callback=initMap">
    </script>
@stop