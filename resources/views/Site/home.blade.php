@extends('Site.layout.master')
@section('title')
    Dalelak
@stop
@section('content')
    <section id="tqniah-content-search">
        <div class="container">
            <div class="Tqniah-title-font">
                <h1 class="wow animate fadeInUp" data-wow-duration="1.5s">ابحث الأن وأحصل على خصمك من مكانك المفضل</h1>
            </div><!-- END Title -->
            <div class="tqniah-search-form">
                <form action="{{route('SEARCH')}}" method="post" enctype="multipart/form-data">
                    {{csrf_field()}}
                    <div class="wow animate fadeInUp" data-wow-duration="2s">
                        <ul>
                            @foreach($categories as $category)
                                <li>
                                    <input type="radio" value="{{$category->name_en}}" id="{{$category->id}}" name="selector">
                                    <div class="check">
                                        {{--<div class="inside"></div>--}}
                                    </div>
                                    <label for="{{$category->id}}">{{$category->name_ar}}</label>
                                </li>
                            @endforeach
                        </ul>
                    </div>
                    <div class="tqniah-search-input wow animate fadeInUp" data-wow-duration="2.5s">

                        <input type="search" name="search" placeholder="إبحث هنا"/>
                        <button>
                            <i class="fa fa-search"></i>
                        </button>
                    </div>
                </form>
                <div class="tqniah-downloads">
                    <h3 class="wow animate fadeInUp" data-wow-duration="3s">يمكنك الأن تحميل تطبيق دليلك والحصول على
                        الخصومات</h3>
                    <div class="wow animate fadeInUp" data-wow-duration="3.5s">
                        <a href="#" class=""><img src="{{asset('site/images/and.png')}}" alt="Android"/></a>
                        <a href="#" class=""><img src="{{asset('site/images/apple.png')}}" alt="apple"/></a>
                    </div>
                </div>
            </div><!-- END Form Search -->
        </div><!-- END Container -->

    </section>


@stop