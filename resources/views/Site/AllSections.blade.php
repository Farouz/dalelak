@extends('Site.layout.master')
@section('title')
    كل الاقسام
@stop
@section('content')
    <div class="clearfix"></div>
    <div class="tqniah-title-page Tqniah-title-font">
        <h1 class="wow animate pulse" data-wow-duration="1.5s">الأقسام الرئيسية</h1>
    </div>
    <section id="tqniah-content-cat">
        <div class="container">
            <div class="tqniah-categories-div">
                @foreach($sections as $section)
                    <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                        <div class="tqniah-animate wow animate slideInUp" data-wow-duration="1.5s">
                            <div class="tqniah-single-categories">
                                <a href="{{route('GET_IN_SECTION',$section->id)}}">
                                    <div class="tqniah-img-cat">
                                        <span></span>
                                        <img src="{{asset('assets/images/'.$section->avatar)}}"
                                             alt="{{$section->name_ar}}"/>
                                        <span></span>
                                    </div>
                                    <h4>
                                        {{$section->name_ar}}                                </h4>
                                </a>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </section>



@stop