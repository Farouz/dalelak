<!DOCTYPE html>
<html lang="ar">
<head>
    <meta charset="UTF-8">
    <meta name="description" content="">
    <meta name="keywords" content="">
    <meta name="author" content="">
    <meta name="robots" content="index/follow">

    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
    <meta name="HandheldFriendly" content="true">

    <title>@yield('title')</title>
    <link rel="shortcut icon" type="image/png" href="{{asset('site/images/logo.png')}}">

    <meta name="theme-color" content="#82b044">
    <meta name="msapplication-navbutton-color" content="#82b044">
    <meta name="apple-mobile-web-app-status-bar-style" content="#82b044">

    <link rel="stylesheet" type="text/css" href="{{asset('site/css/bootstrap.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('site/css/font-awesome.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('site/css/owl.carousel.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('site/css/owl.theme.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('site/css/owl.transitions.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('site/css/animate.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('site/css/hover-min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('site/style.css')}}">
    @yield('css')
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <![endif]-->
</head>

<body id="top">
<!--===============================
	LOADER
===================================-->
<div class="pre-load">
    <div class="loader">
        <span class="loader-block"></span>
        <span class="loader-block"></span>
        <span class="loader-block"></span>
        <span class="loader-block"></span>
        <span class="loader-block"></span>
        <span class="loader-block"></span>
        <span class="loader-block"></span>
        <span class="loader-block"></span>
        <span class="loader-block"></span>
    </div>
</div>
<div class="tqnih-bg-body-home">
    <div class="tqniah-bg-color-home">
        <!--===============================
            HEADER
        ===================================-->

        <section id="tqniah-header">
            <div class="container">
                <div class="col-lg-9 col-md-9 col-sm-9 col-xs-12">
                    <div class="tqniah-navbar">
                        <div class="nav-tqniah">
                            <nav>
                                <button type="button" class="hidden-lg hidden-md hidden-sm">
                                    <span></span>
                                    <span></span>
                                    <span></span>
                                </button>
                                <a class="tqniah-logo-site">
                                    <img src="{{asset('site/images/logo.png')}}" alt="logo site by tqniah"/>
                                </a>
                                <div class="nav-tqniah-list-item">
                                    <ul>
                                        <li class="active"><a href="{{route('HOME')}}">الرئيسية</a></li>
                                        <li><a href="{{route('GET_ALL_SECTIONS')}}">الأقسام الرئيسية</a></li>
                                        <li><a href="https://www.fb.com/{{$setting->contact_facebook}}">تواصل معنا</a></li>
                                    </ul>
                                </div>
                            </nav>
                        </div>
                    </div><!-- END NAV -->
                </div>
                {{--<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">--}}
                {{--<div class="row" id="tqinah-counter">--}}
                {{--<div class="tqniah-count">--}}
                {{--<p>عدد الزيارات : <span class="counter-value" data-count="124125167">0</span></p>--}}
                {{--</div>--}}
                {{--</div>--}}
                {{--</div><!-- END Visite -->--}}
            </div><!-- END Container -->
        </section><!-- END Header -->

        <!--===============================
            Content
        ===================================-->
        @yield('content')
        <footer>
            <h3>
                جميع الحقوق محفوظة لتطبيق دليلك
            </h3>
        </footer>
    </div><!-- END BG color Body -->
</div><!-- END BG Image Body -->


<!--===============================
    SCRIPT
===================================-->

<script src="{{asset('site/js/jquery-1.11.1.min.js')}}"></script>
<script src="{{asset('site/js/bootstrap.min.js')}}"></script>
<script src="{{asset('site/js/owl.carousel.min.js')}}"></script>
<script src="{{asset('site/js/wow.min.js')}}"></script>
<script src="{{asset('site/js/script.js')}}"></script>
@yield('scripts')

<style>
    /* Body styling */
    .nav-tqniah {
    }

    .nav-tqniah nav {
        overflow: hidden;
        padding: 0px;
    }

    .nav-tqniah nav ul {
        list-style: none;
        float: right;
        margin: 0;
        margin-top: 30px;
    }

    .nav-tqniah nav ul li {
        display: inline-block;
    }

    .nav-tqniah nav ul li a {
        padding: 10px;
        color: #fff;
        font-size: 16px;
        font-weight: bold;
    }

    .nav-tqniah nav button {
    }

    .nav-tqniah nav button span {
        height: 5px;
        width: 30px;
        background-color: #000;
        display: block;
        margin-bottom: 2px;
    }

    .nav-tqniah nav ul li a:hover, .nav-tqniah nav ul li.active a {
        color: #8bc34a;
        border-color: #fff;
        border-radius: 0;
    }

    .nav-tqniah-list-item {
    }

    .tqniah-overlay-hidden {
    }

    .tqniah-logo-site {
        float: right;
        height: auto;
        padding: 10px;
    }

    .tqniah-logo-site img {
        height: 70px;
    }

    @media (max-width: 767px) {
        .nav-tqniah {
        }

        .nav-tqniah nav {
        }

        .nav-tqniah nav ul {
            list-style: none;
            padding: 0;
            float: none;
        }

        .nav-tqniah nav ul li {
            display: block;
            border-bottom: 1px solid #0b102d;

        }

        .nav-tqniah nav ul li:last-child {
            border-bottom: 0;
        }

        .nav-tqniah nav ul li a {
            display: block;
            padding: 12px 8px;
        }

        .nav-tqniah nav ul li:hover,
        .nav-tqniah nav ul li:focus,
        .nav-tqniah nav ul li:active {
            background-color: #6a9538;
        }

        .nav-tqniah nav ul li a:hover, .nav-tqniah nav ul li.active a {
            color: #fff;
            background-color: #8bc34a;
        }

        .nav-tqniah nav button {
            float: left;
            margin: 30px;
        }

        .nav-tqniah nav button span {
            height: 3px;
            width: 30px;
            background-color: #fff;
            margin-bottom: 2px;
            margin-bottom: 5px;
        }

        .nav-tqniah-list-item {
            background-color: #03010c;
            position: fixed;
            top: 0;
            height: 100%;
            z-index: 999999999999999999;
            max-width: 320px;
            width: 320px;
            right: -400px;
            box-shadow: 1px 1px 15px #7d7d7d;
        }

        .tqniah-overlay-hidden {
            width: 100%;
            height: 100%;
            background-color: #0000003d;
            position: absolute;
            top: 0;
            z-index: 9999999;
        }
    }

    @media screen and (min-width: 768px) and (max-width: 1024px) {
        .nav-tqniah {
        }

        .nav-tqniah-list-item {
        }

        .nav-tqniah nav {
        }

        .nav-tqniah nav ul {
        }

        .nav-tqniah nav ul li {
        }

        .nav-tqniah nav ul li a {
        }

        .nav-tqniah nav button {
        }

        .nav-tqniah nav button span {
            height: 5px;
            width: 30px;
            background-color: #000;
            display: block;
            margin-bottom: 2px;
        }
    }


</style>
</body>
</html>