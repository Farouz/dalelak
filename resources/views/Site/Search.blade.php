@extends('Site.layout.master')
@section('title')
    نتائج البحث
    @stop
@section('content')


    <div class="clearfix"></div>
    <div class="tqniah-title-page Tqniah-title-font">
        <h1 class="wow animate pulse" data-wow-duration="1.5s">نتائج البحث</h1>
    </div>



    <section id="tqniah-content-coffees">
        <div class="container">
            <div class="tqniah-coffees-div">

                    @foreach($search as $item)
                        @if($item->User->active == 1 )
                            <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                                <div class="tqniah-single-caffees">
                                    <div class="tqniah-img-cafe">
                                        <img src="{{asset('assets/images/'.$item->avatar)}}" alt="{{$item->title}}"/>
                                        <span class="tqniah-discount wow animate fadeInLeft">
                                        @if(!$item->discount == null )
                                                15%
                                                <br/>
                                                خصم
                                            @else
                                                <small>                                        لا  تخفيض
</small>
                                            @endif
						</span>
                                    </div>
                                    <div class="tqniah-text-cafe">
                                        <div class="tqniah-subtext-cafe">
                                            <h4>{{$item->title}}</h4>
                                            <p>{{substr($item->mini_desc,0,50)}}</p>

                                            <small class="tqniah-hidden-di">
                                                <ul>
                                                    <li class="dropdown">
                                                        <a href="#" class="dropdown-toggle" data-toggle="dropdown"
                                                           role="button"
                                                           aria-haspopup="true" aria-expanded="false">
                                                            <i class="fa fa-ellipsis-v" aria-hidden="true"></i>
                                                        </a>
                                                        <ul class="dropdown-menu dropdown-menu-right">
                                                            <li>
                                                                <a href="{{route('GET_SUB_DETAILS',$item->id)}}">التفاصيل</a>
                                                            </li>
                                                        </ul>
                                                    </li>
                                                </ul>
                                            </small>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endif
                    @endforeach

            </div>
        </div>
    </section>

    @stop