@extends('Dashboard.layout.master')
@section('content')
    <h4 class="custom-modal-title">تعديل البيانات</h4>
    <div class="custom-modal-text text-left">
        <form action="{{route('POST_EDIT_CLIENT',$client->id)}}" role="form" method="post" enctype="multipart/form-data">
            {{csrf_field()}}
            <div class="form-group">
                <label for="name">الاسم</label>
                <input type="text" class="form-control" id="name" name="name" value="{{$client->name}}">
            </div>

            <div class="form-group">
                <label for="email">البريد الاليكتروني</label>
                <input type="email" name="email" class="form-control" id="email" value="{{$client->email}}">
            </div>
            <div class="form-group">
                <label for="phone">رقم الهاتف</label>
                <input type="text" name="phone" class="form-control" id="phone" value="{{$client->phone}}">
            </div>

            <div class="form-group">
                <label for="password">الرقم السري</label>
                <i onmousemove="document.getElementById('password').type='text'"  onmouseleave="document.getElementById('password').type='password'" class="fa fa-eye"
                   data-toggle="tooltip"
                   title="اظهار الباسورد "></i>
                <input type="password" class="form-control" id="password" name="password" value="{{$client->password}}">
            </div>

            <div class="form-group">
                <label for="position">حاله التفعيل</label>
                <br>
                <select name="active" style="

    border: 0;
    outline: none;
    color: green;
    border-bottom: 1px solid green;
    height: 40px;
    width: 100px;
">
                    <option value="{{$client->active}}">@if($client->active == 1)
                    مفعل
                                                          @elseif($client->active == 0)
                        غير مفعل
                                                          @endif
                    </option>
                    <option value="0">غير مفعل</option>
                    <option value="1">مفعل</option>
                </select>
            </div>
            <div class="form-group">
                <label for="avatar">االصوره الشخصيه</label>
                <div><img style="width: 160px" src="{{asset('assets/images/'.$client->avatar)}}"></div>
                <input type="file" name="avatar">
            </div>

            <button type="submit" class="btn btn-default waves-effect waves-light">تعديل</button>
        </form>
        <br>
        <br>
        <br>
        <a href="{{route('GET_ALL_CLIENTS')}}">   <button type="button" class="btn btn-primary" style="float: right ; width: 170px">الذهاب الي كل العملاء</button></a>
        <a href="{{route('DASHBOARD')}}">  <button type="button" class="btn btn-primary" style="float: left ;width: 170px">  عوده الي الرئيسيه </button></a>
    </div>


@stop
