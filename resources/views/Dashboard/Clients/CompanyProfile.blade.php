@extends('Dashboard.layout.master')
@section('content')
@section('cssFiles')
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
    <style>
        .mySlides {
            display: none;
        }
    </style>
@stop
@section('content')
    <strong>
        <center>البيانات الكامله الخاصه بشركه {{$company->title}}</center>
    </strong>
    <br>


    <div class="text-center">
        <div>
            <img src="{{asset('assets/images/'.$company->avatar)}}" alt="" width="350"
                 height="200">
        </div>
    </div>
    <br>
    <br>
    <br>
    <div class="container">
        <label for="title" style="float: right"><i class="fa fa-user"></i> أسم الشركه : </label>
        <p>{{$company->title}}</p>
    </div>
    <br>
    <div class="container">
        <label for="title" style="float: right"><i class="fa fa-map"></i> العنوان : </label>
        <p>{{$company->address}}</p>
    </div>
    <br>

    <div class="container">
        <label for="title" style="float: right"><i class="fa fa-phone"></i> رقم التليفون : </label>
        <p>{{$company->phone}}</p>
    </div>
    <br>
    <div class="container">
        <label for="title" style="float: right"><i class="fa fa-user-secret"></i> اسم المالك : </label>
        <p>{{App\User::where('id',$company->user_id)->first()->name}}</p>
    </div>
    <br>
    <div class="container">
        <label for="title" style="float: right"><i class="fa fa-align-justify"></i> القسم : </label>
        <p>{{App\Category::where('id',$company->cat_id)->first()->name_ar}}</p>
    </div>
    <br>
    <div class="container">
        <label for="title" style="float: right"><i class="fa fa-money-bill-alt"></i> التخفيض الحالي : </label>
        <p> %{{$company->discount}} </p>
    </div>
    <br>
    <div class="container">
        <label for="title" style="float: right"><i class="fa fa-users"></i> عدد الزيارات : </label>
        <p>  {{$company->visitors}} زياره </p>
    </div>
    <br>
    <div class="container">
        <label for="title" style="float: right"><i class="fa fa-comments"></i> الوصف المبدئي : </label>
        <br>
        <div class="panel panel-border panel-custom">
            <div class="panel-heading">
                <h3 class="panel-title">الوصف المصغر للشركه</h3>
            </div>
            <div class="panel-body">
                <p>
                    {{$company->mini_desc}}
                </p>
            </div>
        </div>

    </div>
    <br>
    <div class="container">
        <label for="title" style="float: right"><i class="fa fa-comments"></i> الوصف الكامل : </label>
        <br>
        <div class="panel panel-border panel-info">
            <div class="panel-heading">
                <h3 class="panel-title">الوصف الكامل للشركه</h3>
            </div>
            <div class="panel-body">
                <p>
                    {{$company->main_desc}}
                </p>
            </div>
        </div>
    </div>
    <br>

    <div class="container">
        <label for="title" style="float: right"><i class="fa fa-users"></i> الصور الخاصه بالشركه : </label>

        @foreach(explode(' ',$company->sliders) as $slide)
            <div class="w3-content w3-section" style="max-width:500px">
                <img class="mySlides" src="{{asset('assets/images/'.$slide)}}" style="width:100%">
            </div>
        @endforeach
    </div>

@stop

@section('scripts')
    <script>
        var myIndex = 0;
        carousel();

        function carousel() {
            var i;
            var x = document.getElementsByClassName("mySlides");
            for (i = 0; i < x.length; i++) {
                x[i].style.display = "none";
            }
            myIndex++;
            if (myIndex > x.length) {
                myIndex = 1
            }
            x[myIndex - 1].style.display = "block";
            setTimeout(carousel, 3500);
        }
    </script>
@stop