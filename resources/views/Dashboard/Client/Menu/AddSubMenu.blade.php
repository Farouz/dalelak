@extends('Dashboard.Client.layout.master')
@section('content')
    <h4 class="custom-modal-title"> اضافه منتجات جديد خاص بـ {{$menu->title}}
    </h4>

    <div class="custom-modal-text text-left">
        <form action="{{route('POST_ADD_SUB_MENU')}}" role="form" method="post" enctype="multipart/form-data">
            {{csrf_field()}}

            <input type="hidden" name="menu_id" value="{{$menu->id}}">


            <div class="form-group">
                <label for="title">الاسم</label>
                <input type="text" class="form-control" id="title" name="title" placeholder="اسم المنتج " required>
            </div>
            <div class="form-group">
                <label for="salary">السعر</label>
                <input type="text" class="form-control" id="salary" name="salary" placeholder="السعر" required>
            </div>

            <div class="form-group">
                <label for="available">متاح </label>
                <br>
                <select name="available" style="

    border: 0;
    outline: none;
    color: green;
    border-bottom: 1px solid green;
    height: 40px;
    width: 100px;
" required>
                    <option value="1">متاح</option>
                    <option value="0">غير متاح</option>
                </select>
            </div>


            <div class="form-group">
                <label for="avatar">الصوره</label>
                <input type="file" name="avatar" id="avatar" placeholder="الصوره الخاصه بالمنتج"
                       required>
            </div>


            <button type="submit" class="btn btn-default waves-effect waves-light">تسجيل</button>
        </form>
    </div>
@stop