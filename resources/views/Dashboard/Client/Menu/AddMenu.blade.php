@extends('Dashboard.Client.layout.master')
@section('content')
    <h4 class="custom-modal-title"> اضافه منيو جديد خاص بـ {{$company->title}}</h4>
    <div class="custom-modal-text text-left">
        <form action="{{route('POST_ADD_MENU')}}" role="form" method="post" enctype="multipart/form-data">
            {{csrf_field()}}

            <input type="hidden" name="company_id" value="{{$company->id}}">


            <div class="form-group">
                <label for="title">الاسم</label>
                <input type="text" class="form-control" id="title" name="title" placeholder="اسم القسم " required>
            </div>

            <div class="form-group">
                <label for="avatar">الصوره</label>
                <input type="file" name="avatar"  id="avatar" placeholder="الصوره الخاصه بالقسم"
                       required>
            </div>


            <button type="submit" class="btn btn-default waves-effect waves-light">تسجيل</button>
        </form>
    </div>
@stop