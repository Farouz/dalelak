@extends('Dashboard.Client.layout.master')
@section('content')
    <h4 class="custom-modal-title"> تعديل المحتوي </h4>
    <div class="custom-modal-text text-left">
        <form action="{{route('POST_EDIT_SUB_MENU',$sub_menu->id)}}" role="form" method="post" enctype="multipart/form-data">
            {{csrf_field()}}
            <div class="form-group">
                <label for="title">الاسم</label>
                <input type="text" class="form-control" id="title" name="title" value="{{$sub_menu->title}}">
            </div>
            <div class="form-group">
                <label for="salary">السعر</label>
                <input type="text" class="form-control" id="salary" name="salary" value="{{$sub_menu->salary}}">
            </div>

            <div class="form-group">
                <label for="available">متاح </label>
                <br>
                <select name="available" style="

    border: 0;
    outline: none;
    color: green;
    border-bottom: 1px solid green;
    height: 40px;
    width: 100px;
" required>
                    <option value="{{$sub_menu}}">@if($sub_menu->available == 1) متاح
                    @elseغير متاح
                    @endif</option>
                    <option value="1">متاح</option>
                    <option value="0">غير متاح</option>
                </select>
            </div>


            <div class="form-group">
                <label for="avatar">الصوره</label>
                <input type="file" name="avatar" id="avatar" value="{{$sub_menu->avatar}}"
                >
            </div>


            <button type="submit" class="btn btn-default waves-effect waves-light">تسجيل</button>
        </form>
    </div>
@stop