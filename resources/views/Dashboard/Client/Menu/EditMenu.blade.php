@extends('Dashboard.Client.layout.master')
@section('content')
    <h4 class="custom-modal-title"> تعديل المنيو </h4>
    <div class="custom-modal-text text-left">
        <form action="{{route('POST_EDIT_MENU',$menu->id)}}" role="form" method="post" enctype="multipart/form-data">
            {{csrf_field()}}
            <div class="form-group">
                <label for="title">الاسم</label>
                <input type="text" class="form-control" id="title" name="title" value="{{$menu->title}}">
            </div>

            <div class="form-group">
                <label for="avatar">الصوره</label>
                <input type="file" name="avatar" id="avatar" value="{{$menu->avatar}}"
                >
            </div>


            <button type="submit" class="btn btn-default waves-effect waves-light">تسجيل</button>
        </form>
    </div>
@stop