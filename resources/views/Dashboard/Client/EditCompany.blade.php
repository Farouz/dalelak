@extends('Dashboard.Client.layout.master')
@section('cssFiles')
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
    <style>
        .mySlides {display:none;}
    </style>
    @stop
@section('content')
    <h4 class="custom-modal-title"> تعديل معلومات عن شركه {{$company->title}}</h4>
    <div class="custom-modal-text text-left">
        <form action="{{route('POST_EDIT_COMPANY',$company->id)}}" role="form" method="post" enctype="multipart/form-data">
            {{csrf_field()}}
            <div class="form-group">
                <label for="title">الاسم</label>
                <input type="text" class="form-control" id="title" name="title" value="{{$company->title}}">
            </div>

            <div class="form-group">
                <label for="address">العنوان</label>
                <input type="text" name="address" class="form-control" id="address" value="{{$company->address}}">
            </div>

            <div class="form-group">
                <label for="position">القسم التابع له</label>
                <br>
                <select name="cat_id" style="

    border: 0;
    outline: none;
    color: black;
    border-bottom: 1px solid green;
    height: 40px;
    width: 150px;


" >
                    <option value="{{$company->cat_id}}" selected>{{App\Category::where('id',$company->cat_id)->first()->name_ar}}</option>
                    @foreach($cats as $cat)
                        <option value="{{$cat->id}}">{{$cat->name_ar}}</option>
                    @endforeach
                </select>
            </div>
            <div class="form-group">
                <label for="phone"> رقم الهاتف</label>
                <input type="text" name="phone" class="form-control" id="phone" value="{{$company->phone}}">
            </div>
            <div class="form-group">
                <label for="discount">التخفيض الحالي</label>
                <input type="text" name="discount" class="form-control" id="discount"   value="{{$company->discount}}">
            </div>

            @include('Dashboard.Client.map')
            <div id="myMap">
            </div>
            <input id="address" type="text" style="width: 600px"> <br>
            <input type="text" name="lat" id="latitude" value="{{$company->lat}}" >
            <input type="text" name="lan" id="longitude" value="{{$company->lan}}">
            <div class="form-group">
                <label for="avatar">االصوره الرئيسيه</label>
                <br>
                <input type="file" name="avatar" value="{{$company->avatar}}">
            </div>
            <div class="form-group">
                <label for="avatar">صور الشركه</label>
                <br>
                <input type="file" name="sliders[]" value="{{$company->sliders}}" multiple>
            </div>
            <div class="clearfix"></div>

            <div class="w3-content w3-display-container">
                <h2 class="w3-center">صور العرض </h2>

            @foreach(explode(' ',$company->sliders) as $slide)
                    <div class="w3-content w3-section" style="max-width:500px">
                        <img class="mySlides" src="{{asset('assets/images/'.$slide)}}" style="width:100%">
                    </div>
                @endforeach
            </div>

            <div class="form-group">
                <label for="mini-desc">نبذه ملخصه عن الشركه</label>
                <br>
                <textarea name="mini_desc" id="mini-desc" style="width: 700px;height: 120px">{{$company->mini_desc}}</textarea>
            </div>
            <div class="form-group">
                <label for="main-desc">نبذه ملخصه عن الشركه</label>
                <br>
                <textarea name="main_desc" id="main-desc" style="width: 700px;height: 150px">{{$company->main_desc}}</textarea>
            </div>

            <button type="submit" class="btn btn-default waves-effect waves-light">تسجيل</button>
        </form>
    </div>
@stop

@section('scripts')
    <script>
        var myIndex = 0;
        carousel();

        function carousel() {
            var i;
            var x = document.getElementsByClassName("mySlides");
            for (i = 0; i < x.length; i++) {
                x[i].style.display = "none";
            }
            myIndex++;
            if (myIndex > x.length) {myIndex = 1}
            x[myIndex-1].style.display = "block";
            setTimeout(carousel, 3500);
        }
    </script>
    @stop