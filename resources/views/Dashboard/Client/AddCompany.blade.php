@extends('Dashboard.Client.layout.master')
@section('content')
    <h4 class="custom-modal-title"> اضافه شركه جديده</h4>
    <div class="custom-modal-text text-left">
        <form action="{{route('POST_ADD_COMPANY')}}" role="form" method="post" enctype="multipart/form-data">
            {{csrf_field()}}
            <div class="form-group">
                <label for="title">الاسم</label>
                <input type="text" class="form-control" id="title" name="title" placeholder="اسم الشركه " required>
            </div>
            <input type="hidden" name="user_id" value="{{$client->id}}">

            <div class="form-group">
                <label for="address">العنوان</label>
                <input type="text" name="address" class="form-control" id="address" placeholder="العنوان"
                       required>
            </div>

            <div class="form-group">
                <label for="position">القسم التابع له</label>
                <br>
                <select name="cat_id" style="

    border: 0;
    outline: none;
    color: black;
    border-bottom: 1px solid green;
    height: 40px;
    width: 150px;


" required>
                    @foreach($cats as $cat)
                        <option value="{{$cat->id}}">{{$cat->name_ar}}</option>
                    @endforeach
                </select>
            </div>
            <div class="form-group">
                <label for="phone"> رقم الهاتف</label>
                <input type="text" name="phone" class="form-control" id="phone" placeholder="رقم الهاتف"
                       required>
            </div>
            <div class="form-group">
                <label for="discount">التخفيض الحالي</label>
                <input type="text" name="discount" class="form-control" id="discount" placeholder="التخفيض الحالي"
                       required>
            </div>

            @include('Dashboard.Client.map')
            <div id="myMap">
            </div>
            <input id="address" type="text" style="width: 600px"> <br>
            <input type="text" name="lat" id="latitude" placeholder="latitude">
            <input type="text" name="lan" id="longitude" placeholder="longitude">
            <div class="form-group">
                <label for="avatar">االصوره الرئيسيه</label>
                <br>
                <input type="file" name="avatar" required>
            </div>
            <div class="form-group">
                <label for="avatar">صور الشركه</label>
                <br>
                <input type="file" name="sliders[]" required multiple>
            </div>
            <div class="form-group">
                <label for="mini-desc">نبذه ملخصه عن الشركه</label>
                <br>
                <textarea name="mini_desc" id="mini-desc" style="width: 700px;height: 120px"></textarea>
            </div>
            <div class="form-group">
                <label for="main-desc">نبذه ملخصه عن الشركه</label>
                <br>
                <textarea name="main_desc" id="main-desc" style="width: 700px;height: 150px"></textarea>
            </div>

            <button type="submit" class="btn btn-default waves-effect waves-light">تسجيل</button>
        </form>
    </div>
@stop
