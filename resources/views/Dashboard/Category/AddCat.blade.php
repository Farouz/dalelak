@extends('Dashboard.layout.master')
@section('content')
    <h4 class="custom-modal-title">إضافه قسم جديد</h4>
    <div class="custom-modal-text text-left">
        <form action="{{route('POST_ADD_CATEGORY')}}" role="form" method="post" enctype="multipart/form-data">
            {{csrf_field()}}
            <div class="form-group">
                <label for="name_ar">الاسم</label>
                <input type="text" class="form-control" id="name_ar" name="name_ar" placeholder="الاسم بالعربي"
                       required>
            </div>
            <div class="form-group">
                <label for="name_en">الاسم باللغه الانجليزيه</label>
                <input type="text" name="name_en" class="form-control" id="name_en" placeholder="الاسم بالانجليزي"
                       required>
            </div>
            <div class="form-group">
                <label for="avatar">االصوره </label>
                <br>
                <input type="file" name="avatar" required>
            </div>
            <button type="submit" class="btn btn-default waves-effect waves-light">تخزين</button>
        </form>
    </div>
@stop