@extends('Dashboard.layout.master')
@section('content')
    <h4 class="custom-modal-title">تعديل البيانات</h4>
    <div class="custom-modal-text text-left">
        <form action="{{route('POST_EDIT_CATEGORY',$cat->id)}}" role="form" method="post" enctype="multipart/form-data">
            {{csrf_field()}}
            <div class="form-group">
                <label for="name_ar">الاسم</label>
                <input type="text" class="form-control" id="name_ar" name="name_ar" value="{{$cat->name_ar}}"
                       required>
            </div>
            <div class="form-group">
                <label for="name_en">الاسم باللغه الانجليزيه</label>
                <input type="text" name="name_en" class="form-control" id="name_en" value="{{$cat->name_en}}"
                       required>
            </div>
            <div class="form-group">
                <label for="avatar">االصوره </label>
                <div><img style="width: 160px" src="{{asset('assets/images/'.$cat->avatar)}}"></div>
                <input type="file" name="avatar">
            </div>
            <button type="submit" class="btn btn-default waves-effect waves-light">تعديل</button>
        </form>
    </div>
@stop