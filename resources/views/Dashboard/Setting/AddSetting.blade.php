@extends('Dashboard.layout.master')

@section('content')

    <h4 class="custom-modal-title">إضافه الاعدادات</h4>
    <div class="custom-modal-text text-left">


        <form action="{{route('POST_ADD_SETTING')}}" role="form" method="post" enctype="multipart/form-data">
            {{csrf_field()}}
            <div class="form-group">
                <label for="site_title">أسم الموقع </label>
                <input type="text" class="form-control" id="site_title" name="site_title" placeholder="أسم الموقع  "
                       required>
            </div>

            <div class="form-group">
                <label for="IOS_link">لينك التطبيق ل IOS</label>
                <input type="text" name="IOS_link" class="form-control" id="IOS_link" placeholder="لينك التطبيق ل IOS"
                       required>
            </div>
            <div class="form-group">
                <label for="Android_link">لينك التطبيق ل Android</label>
                <input type="text" name="Android_link" class="form-control" id="Android_link"
                       placeholder="لينك التطبيق ل Android"
                       required>
            </div>
            <div class="form-group">
                <label for="contact_facebook">لينك صفحه FaceBook الخاصه بالموقع</label>
                <input type="text" name="contact_facebook" class="form-control" id="contact_facebook"
                       placeholder="لينك صفحه الFacebook"
                       required>
            </div>

            <div class="form-group">
                <label for="logo">صوره اللوجو </label>
                <br>
                <input type="file" name="logo" required>
            </div>

            <div class="form-group">
                <label for="logo">صوره الخلفيه </label>
                <br>
                <input type="file" name="background_image" required>
            </div>


            <button type="submit" class="btn btn-default waves-effect waves-light">تسجيل</button>
        </form>
    </div>

@stop
