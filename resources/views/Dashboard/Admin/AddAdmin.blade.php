@extends('Dashboard.layout.master')
@section('content')
    <h4 class="custom-modal-title">إضافه أدمن جديد</h4>
    <div class="custom-modal-text text-left">
        <form action="{{route('POST_ADD_ADMIN')}}" role="form" method="post" enctype="multipart/form-data">
            {{csrf_field()}}
            <div class="form-group">
                <label for="name">الاسم</label>
                <input type="text" class="form-control" id="name" name="name" placeholder="اسم الادمن" required>
            </div>
            <div class="form-group">
                <label for="email">البريد الاليكتروني</label>
                <input type="email" name="email" class="form-control" id="email" placeholder="البريد الاليكتروني"
                       required>
            </div>

            <div class="form-group">
                <label for="password">الرقم السري</label>
                <i onmousemove="document.getElementById('password').type='text'"  onmouseleave="document.getElementById('password').type='password'" class="fa fa-eye"
                   data-toggle="tooltip"
                   title="اظهار الباسورد "></i>
                <input type="password" class="form-control" id="password" name="password" required>
            </div>

            <div class="form-group">
                <label for="position">المسئوليه</label>
                <br>
                <select name="role" style="

    border: 0;
    outline: none;
    color: green;
    border-bottom: 1px solid green;
    height: 40px;
    width: 100px;
" required>
                    <option value="0">مشرف</option>
                    <option value="1">مدير</option>
                </select>
            </div>
            <div class="form-group">
                <label for="avatar">االصوره الشخصيه</label>
                <br>
                <input type="file" name="avatar" required>
            </div>

            <button type="submit" class="btn btn-default waves-effect waves-light">تسجيل</button>
        </form>
    </div>


@stop
