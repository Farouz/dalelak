@extends('Dashboard.layout.master')
@section('content')
    <h4 class="custom-modal-title">تعديل البيانات</h4>
    <div class="custom-modal-text text-left">
        <form action="{{route('POST_EDIT_ADMIN',$admin->id)}}" role="form" method="post" enctype="multipart/form-data">
            {{csrf_field()}}
            <div class="form-group">
                <label for="name">الاسم</label>
                <input type="text" class="form-control" id="name" name="name" value="{{$admin->name}}">
            </div>

            <div class="form-group">
                <label for="email">البريد الاليكتروني</label>
                <input type="email" name="email" class="form-control" id="email" value="{{$admin->email}}">
            </div>
            <div class="form-group">
                <label for="password">الرقم السري</label>
                <i onmousemove="document.getElementById('password').type='text'"  onmouseleave="document.getElementById('password').type='password'" class="fa fa-eye"
                   data-toggle="tooltip"
                   title="اظهار الباسورد "></i>
                <input type="password" class="form-control" id="password" name="password" value="{{$admin->password}}">
            </div>

            <div class="form-group">
                <label for="position">المسئوليه</label>
                <br>
                <select name="role" style="

    border: 0;
    outline: none;
    color: green;
    border-bottom: 1px solid green;
    height: 40px;
    width: 100px;
">
                    <option value="{{$admin->role}}">مدير</option>
                    <option value="0">مشرف</option>
                    <option value="1">مدير</option>
                </select>
            </div>
            <div class="form-group">
                <label for="avatar">االصوره الشخصيه</label>
                <div><img style="width: 160px" src="{{asset('assets/images/'.$admin->avatar)}}"></div>
                <input type="file" name="avatar">
            </div>

            <button type="submit" class="btn btn-default waves-effect waves-light">تعديل</button>
        </form>
        <br>
        <br>
        <br>
     <a>   <button type="button" class="btn btn-primary" style="float: right ; width: 170px">الذهاب الي كل المديرين</button></a>
      <a>  <button type="button" class="btn btn-primary" style="float: left ;width: 170px">  عوده الي الرئيسيه </button></a>
    </div>


@stop
