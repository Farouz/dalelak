<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCompaniesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('companies', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned();
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->integer('cat_id')->unsigned();
            $table->foreign('cat_id')->references('id')->on('categories')->onDelete('cascade');
            $table->string('title');
            $table->string('address');
            $table->string('phone');
            $table->string('mini_desc');
            $table->text('main_desc');
            $table->integer('discount')->nullable();
            $table->decimal('lan');
            $table->decimal('lat');
            $table->string('avatar');
            $table->string('sliders');
            $table->integer('visitors')->default(0)->nullable();
            $table->tinyInteger('active')->default(0)->nullable();
            $table->integer('rate')->default(0)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('companies');
    }
}
