<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Programmers extends Model
{
    protected $table = 'programmers';
    protected $fillable = ['name', 'job_title', 'avatar'];

    public function Details()
    {
        return $this->hasMany(Detail::class, 'programmer_id');
    }
}
