<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Rate extends Model
{
    protected $fillable = ['visitor_id', 'company_id', 'rate'];
    protected $table = 'visitors_rates';
}
