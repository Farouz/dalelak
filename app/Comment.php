<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
    protected $fillable = ['name', 'email', 'company_id', 'comment'];
    protected $table = 'comments';

    public function Company()
    {
        return $this->belongsTo(Company::class, 'company_id');
    }
}
