<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Company extends Model
{
    protected $table = 'companies';
    protected $fillable = ['title', 'mini_desc', 'avatar', 'address', 'phone', 'main_desc', 'discount', 'sliders', 'lan', 'lat', 'rate', 'active', 'visitors'];

    public function User()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function Category()
    {
        return $this->belongsTo(Category::class, 'cat_id');
    }

    public function Menus(){
        return $this->hasMany(Menu::class , 'company_id');
    }

    public function Comments(){
        return $this->hasMany(Comment::class , 'company_id');
    }
}
