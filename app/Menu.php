<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Menu extends Model
{
    protected $fillable = ['avatar', 'title', 'company_id'];
    protected $table = 'menus';


    //relation with Company
    public function Company()
    {
        return $this->belongsTo(Company::class, 'company_id');
    }

    //relation with submenus
    public function SubMenus(){
        return $this->hasMany(SubMenu::class , 'menu_id');
    }
}
