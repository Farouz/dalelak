<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SubMenu extends Model
{
    protected $table = 'sub_menus';
    protected $fillable = ['avatar', 'title', 'menu_id', 'available', 'salary'];

    public function Menu()
    {
        return $this->belongsTo(Menu::class, 'menu_id');
    }
}
