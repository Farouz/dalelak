<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Detail extends Model
{
    protected $fillable = ['facebook', 'twitter', 'linkedin', 'instagram', 'programmer_id'];
    protected $table = 'details';

    public function Programmer()
    {
        return $this->belongsTo(Programmers::class, 'programmer_id');
    }
}
