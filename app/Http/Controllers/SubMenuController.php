<?php

namespace App\Http\Controllers;

use App\Menu;
use App\Company;
use App\SubMenu;
use foo\bar;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class SubMenuController extends Controller
{
    //Add Sub Menus Page

    public function getAddSubMenu($id)
    {
        $menu = Menu::findOrFail($id);
        $company = Company::where('id', $menu->company_id)->first();
        if ($company->user_id == Auth::id()) {
            return view('Dashboard.Client.Menu.AddSubMenu', compact('menu', 'company'));
        } else {
            return back();

        }
    }

    public function postAddSubMenu(Request $request)
    {
        $this->validate($request, ['title' => 'required', 'salary' => 'required|numeric|min:1', 'avatar' => 'required|image']);
        $item = new SubMenu();
        $item->title = $request['title'];
        $item->salary = $request['salary'];
        $item->available = $request['available'];
        $item->menu_id = $request['menu_id'];
        if ($request->hasFile('avatar')) {
            $item_avatar = md5($request['avatar']->getClientOriginalName()) . '.' . $request['avatar']->getClientOriginalExtension();
            $request['avatar']->move(public_path('assets/images'), $item_avatar);
            $item->avatar = $item_avatar;
        }
        $item->save();
        return redirect()->back()->with('success', 'تم اضافه المنتج');
    }

    //Edit Sub_Menu
    public function getEditSubMenu($id)
    {
        $sub_menu = SubMenu::find($id);
        return view('Dashboard.Client.Menu.EditSub', compact('sub_menu'));
    }

    //Edit Function
    public function postEditSubMenu($id, Request $request)
    {
        $sub_menu = SubMenu::find($id);
        $menu = Menu::where('id', $sub_menu->menu_id)->first();
        $this->validate($request, ['salary' => 'numeric|min:5', 'avatar' => 'image']);
        $sub_menu->title = $request['title'];
        $sub_menu->salary = $request['salary'];
        $sub_menu->available = $request['available'];

        if ($request->hasFile('avatar')) {
            $item_avatar = md5($request['avatar']->getClientOriginalName()) . '.' . $request['avatar']->getClientOriginalExtension();
            $request['avatar']->move(public_path('assets/images'), $item_avatar);
            $sub_menu->avatar = $item_avatar;
        }
        $sub_menu->update();
        return redirect()->route('SHOW_SUB_MENU', $menu->id)->with('success', 'تم تعديل البيانات');
    }

    public function deleteSubMenu($id)
    {
        SubMenu::destroy($id);
        return redirect()->back()->with('success', 'تم الحذف');
    }
}
