<?php

namespace App\Http\Controllers;

use App\Category;
use App\Company;
use App\Setting;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class SearchController extends Controller
{
    public function __construct()
    {
        $setting = Setting::orderBy('created_at', 'DESC')->first();
        view()->share(array(
            'setting'=>$setting
        ));
    }
    //search
    public function search(Request $request)
    {
        $category = Category::where('name_en', $request['selector'])->first();
        $search =Company::where([
            ['title', 'LIKE', '%'.$request['search'].'%'],
            ['cat_id', '=', $category->id],
        ])->get();

        return view('Site.Search', compact('search'));

    }
}
