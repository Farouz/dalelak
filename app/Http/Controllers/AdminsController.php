<?php

namespace App\Http\Controllers;

use App\Admin;
use Dotenv\Validator;
use foo\bar;
use Illuminate\Http\Request;

class AdminsController extends Controller
{
    public function getDashboard()
    {

        return view('Dashboard.Home.index');
    }

    //Login form Function For Admin
    public function getDashboardLogin()
    {
        return view('Dashboard.Auth.loginForm');
    }

//post login form for admin
    public function postDashboardLogin()
    {
        //guard for admin to auth
        if (!auth()->guard('webadmin')->attempt(\request(['email', 'password']))) {
            return back()->withErrors(['message' => 'Email Or Password Not Correct']);
        };
        return redirect()->route('DASHBOARD')->with('success', 'Welcome ');
    }

    //logout admin
    public function getlogout()
    {
        \auth()->guard('webadmin')->logout();
        return redirect()->route('ADMIN_LOGIN');
    }

    //get All Admins and Moderator
    public function getAllAdmins()
    {
        $admins = Admin::all();
        return view('Dashboard.Admin.AllAdmins', compact('admins'));
    }

    //Delete Moderators
    public function deleteAdmin($id)
    {
        Admin::destroy($id);
        return back()->with('success', 'تم الحذف بنجاح');
    }

    //Edit Admin Page
    public function editAdmin($id)
    {
        $admin = Admin::findOrFail($id);
        return view('Dashboard.Admin.EditAdmin', compact('admin'));
    }

    //Edit Function
    public function postEditAdmin($id, Request $request)
    {
        $admin = Admin::findOrFail($id);
        $this->validate($request, ['email' => 'email', 'avatar' => 'image']);
        $admin->name = $request['name'];
        $admin->email = $request['email'];
        $admin->role = $request['role'];
        $admin->password = bcrypt($request['password']);
        if ($request->hasFile('avatar')) {
            $admin_avatar = md5($request['avatar']->getClientOriginalName()) . '.' . $request['avatar']->getClientOriginalExtension();
            $request['avatar']->move(public_path('assets/images'), $admin_avatar);
            $admin->avatar = $admin_avatar;
        }
        $admin->update();
        return redirect()->route('get_all_admins')->with('success', 'تم التعديل بنجاح');
    }

    //Add Admin Page
    public function getAddAdmin()
    {
        return view('Dashboard.Admin.AddAdmin');
    }

    //Post Function for add ADMIN
    public function postAddAdmin(Request $request)
    {
        $this->validate($request, ['name' => 'required|', 'password' => 'required', 'email' => 'required|email|unique:admins', 'avatar' => 'required|image']);
        $admin = new  Admin();
        $admin->name = $request['name'];
        $admin->password = bcrypt($request['password']);
        $admin->email = $request['email'];
        $admin->role = $request['role'];
        if ($request->hasFile('avatar')) {
            $admin_avatar = md5($request['avatar']->getClientOriginalName()) . '.' . $request['avatar']->getClientOriginalExtension();
            $request['avatar']->move(public_path('assets/images'), $admin_avatar);
            $admin->avatar = $admin_avatar;
        } else {
            $admin->avatar = 'admin_default.jpg';
        }
        $admin->save();
        return redirect()->route('get_all_admins')->with('success', 'تم اضافه الادمن');
    }

    //Make the moderator admin
    public function makeAdmin($id)
    {
        $admin = Admin::findOrFail($id);
        $admin->role = 1;
        $admin->update();
        return back()->with('success', 'تم تحديده أدمن ');
    }
}
