<?php

namespace App\Http\Controllers;

use App\Company;
use App\User;
use Illuminate\Http\Request;

class UsersController extends Controller
{
    //retrieve all Clients
    public function getAllClients()
    {
        $clients = User::all();
        return view('Dashboard.Clients.AllClients', compact('clients'));
    }

    //Add Clients page
    public function getAddClient()
    {
        return view('Dashboard.Clients.AddClient');
    }

    //Add Client Post
    public function postAddClient(Request $request)
    {
        $this->validate($request, ['name' => 'required', 'password' => 'required', 'phone' => 'required|numeric|min:11', 'email' => 'required|email|unique:users', 'avatar' => 'required|image']);
        $client = new User();
        $client->name = $request['name'];
        $client->email = $request['email'];
        $client->phone = $request['phone'];
        $client->password = bcrypt($request['password']);
        $client->active = $request['active'];
        if ($request->hasFile('avatar')) {
            $client_avatar = md5($request['avatar']->getClientOriginalName()) . '.' . $request['avatar']->getClientOriginalExtension();
            $request['avatar']->move(public_path('assets/images'), $client_avatar);
            $client->avatar = $client_avatar;
        }
        $client->save();
        return redirect()->route('GET_ALL_CLIENTS')->with('success', 'تم اضافه العميل بنجاح');
    }

    //Edit Client Page
    protected function getEditClient($id)
    {
        $client = User::findOrFail($id);
        return view('Dashboard.Clients.EditClient', compact('client'));
    }

    //Edit Function
    public function postEditClient(Request $request, $id)
    {
        $client = User::findOrFail($id);
        $this->validate($request, ['phone' => 'numeric|min:11', 'email' => 'email', 'avatar' => 'image']);
        $client->name = $request['name'];
        $client->email = $request['email'];
        $client->phone = $request['phone'];
        $client->active = $request['active'];
        $client->password = bcrypt($request['password']);

        if ($request->hasFile('avatar')) {
            $client_avatar = md5($request['avatar']->getClientOriginalName()) . '.' . $request['avatar']->getClientOriginalExtension();
            $request['avatar']->move(public_path('assets/images'), $client_avatar);
            $client->avatar = $client_avatar;
        } else {
            $client_avatar = $client->avatar;
            $client->avatar = $client_avatar;
        }
        $client->update();
        return redirect()->route('GET_ALL_CLIENTS')->with('success', 'تم تعديل العميل');
    }

    //Delete Client

    public function deleteClient($id){
        User::destroy($id);
        return back()->with('success','تم مسح العميل');
    }

    //Make Client Active
    public function ActiveClient($id){
        $client=User::find($id);
        $client->active = 1 ;
        $client->update();
        return back()->with('success','تم تفعيل العضو');
    }

    //DeActive The Client
    public function DeActiveClient($id){
        $client=User::find($id);
        $client->active = 0 ;
        $client->update();
        return back()->with('success','تم تعطيل العضو');

    }

    //Get the company of clients
    public function getClientsCompany($id){
        $client=User::findOrFail($id);

        return view('Dashboard.Clients.ClientCompany',compact('client'));
    }

    //Get All Details of company
    public function getSubCompany($id){
        $company = Company::find($id);
        return view('Dashboard.Clients.CompanyProfile',compact('company'));
    }
}
