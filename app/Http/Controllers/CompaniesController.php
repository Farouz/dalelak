<?php

namespace App\Http\Controllers;

use App\Category;
use App\Company;
use App\User;
use foo\bar;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class CompaniesController extends Controller
{
    //retrieve the user companies
    public function getMyCompanies($id)
    {
        if (Auth::id() == $id) {
            $companies = Company::where('user_id', $id)->get();
            return view('Dashboard.Client.MyComp', compact('companies'));
        } else {
            return back();
        }
    }

    //Creating Company Page
    public function getAddCompany($id)
    {
        if (Auth::id() == $id) {
            $client = User::findOrFail($id);
            $cats = Category::all();
            return view('Dashboard.Client.AddCompany', compact('client', 'cats'));
        } else {
            return back();
        }
    }

    //Creating Company Function
    public function postAddCompany(Request $request)
    {
        $this->validate($request, ['title' => 'required', 'address' => 'required', 'phone' => 'required|numeric|min:11', 'lat' => 'required', 'lan' => 'required', 'avatar' => 'required|image', 'sliders' => 'required', 'mini_desc' => 'required', 'main_desc' => 'required']);
        $company = new Company();
        $company->title = $request['title'];
        $company->user_id = $request['user_id'];
        $company->cat_id = $request['cat_id'];
        $company->address = $request['address'];
        $company->phone = $request['phone'];
        $company->lat = $request['lat'];
        $company->lan = $request['lan'];
        $company->mini_desc = $request['mini_desc'];
        $company->main_desc = $request['main_desc'];
        if ($request->hasFile('avatar')) {
            $company_avatar = md5($request['avatar']->getClientOriginalName()) . '.' . $request['avatar']->getClientOriginalExtension();
            $request['avatar']->move(public_path('assets/images'), $company_avatar);
            $company->avatar = $company_avatar;
        } else {
            $company->avatar = 'cat_default.jpg';
        }
        if ($request->hasFile('sliders')) {
            foreach ($request->file('sliders') as $slider) {
                $slider_name = md5($slider->getClientOriginalName()) . '.' . $slider->getClientOriginalExtension();
                $slider->move(public_path('assets/images/'), $slider_name);
                $sliders[] = $slider_name;
            }
            $sliders = implode(" ", $sliders);
            $company->sliders = $sliders;
        }
        $company->save();
        return redirect()->route('GET_MY_COMPANIES', Auth::id())->with('success', 'تم اضافه الشركه');


    }

    //Edit Page
    public function getEditCompany($id)
    {
        $cats = Category::all();
        $company = Company::find($id);
        if ($company->user_id == Auth::id()) {
            return view('Dashboard.Client.EditCompany', compact('company', 'cats'));
        } else {
            return back();
        }
    }

    //Edit Function
    public function postEditCompany(Request $request, $id)
    {
        $this->validate($request, ['phone' => 'required|numeric|min:11', 'avatar' => 'image']);
        $company = Company::findOrFail($id);
        if ($request->hasFile('avatar')) {
            $avatar_name = md5($request['avatar']->getClientOriginalName()) . '.' . $request['avatar']->getClientOriginalExtension();
            $request['avatar']->move(public_path('assets/images'), $avatar_name);
            $company->avatar = $avatar_name;
        }
        if ($request->hasFile('sliders')) {
            foreach ($request->file('sliders') as $slider) {
                $slider_name = md5($slider->getClientOriginalName()) . '.' . $slider->getClientOriginalExtension();
                $slider->move(public_path('assets/images/'), $slider_name);
                $sliders[] = $slider_name;
            }
            $sliders = implode(" ", $sliders);
            $company->sliders = $sliders;
        }
        Company::where('id', $id)->update(['title' => $request['title'],
            'phone' => $request['phone'],
            'address' => $request['address'],
            'lat' => $request['lat'],
            'lan' => $request['lan'],
            'discount' => $request['discount'],
            'mini_desc' => $request['mini_desc'],
            'main_desc' => $request['main_desc'],
            'cat_id' => $request['cat_id'],]
        );

        $company->update();
        return redirect()->route('GET_MY_COMPANIES', Auth::id())->with('success', 'تم تعديل الشركه ');
    }

    //Delete Company
    public function DeleteCompany($id)
    {
        $company = Company::find($id);
        if ($company->user_id == Auth::id()) {
            Company::destroy($id);
            return back()->with('success', 'تم حذف الشركه ');
        } else {
            return back();
        }

    }

    //Company Profile
    public function getShowCompany($id)
    {
        $company = Company::findOrFail($id);
        return view('Dashboard.Client.CompanyProfile', compact('company'));
    }

}
