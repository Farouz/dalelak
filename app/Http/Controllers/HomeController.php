<?php

namespace App\Http\Controllers;

use App\Category;
use App\Comment;
use App\Company;
use App\Setting;
use App\User;
use Illuminate\Http\Request;

class HomeController extends Controller
{

    public function __construct()
    {
        $setting = Setting::orderBy('created_at', 'DESC')->first();
        view()->share(array(
            'setting'=>$setting
        ));
    }

    //Home Function
    public function getIndex()
    {
        $categories = Category::all();
        return view('Site.home',compact('categories'));
    }

    //get All Categories
    public function getAllSections()
    {

        $sections = Category::all();
        return view('Site.AllSections', compact('sections'));
    }

    //Sub Sections
    public function getSubSections($id)
    {

        $category = Category::find($id);
        if ($category) {
            $companies = Company::where('cat_id', $category->id)->get();
            return view('Site.SubSections', compact('category', 'companies'));
        } else {
            return back();
        }
    }

    //Get Single Sub Section Page
    public function getSingleSub($id)
    {
        $company = Company::find($id);
        if ($company) {
            if ($company->User->active == 1) {
                return view('Site.SingleSub', compact('company'));
            }
            return back();
        }
        return back();
    }

    //Send Comment Function
    public function postSentComment(Request $request, $id)
    {
        $this->validate($request, ['name' => 'required', 'email' => 'required|email', 'comment' => 'required']);
        $company = Company::find($id);
        $comment = new Comment();
        $comment->company_id = $company->id;

        $comment->name = $request['name'];
        $comment->email = $request['email'];
        $comment->comment = $request['comment'];
        $comment->save();
        return back();

    }

}
