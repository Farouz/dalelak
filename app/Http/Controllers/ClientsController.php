<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ClientsController extends Controller
{
    //login page
    public function getLogin(){
        return view('Dashboard.Client.LoginForm');
    }

    //Login Function
    public function postLogin(Request $request){
        if (!auth()->attempt(\request(['email', 'password']))) {
            return back()->withErrors(['message' => 'Email Or Password Not Correct']);
        };

        return redirect()->route('Client_Dashboard')->with('success', 'Welcome ');
    }
    public function getDashboard(){
        return view('Dashboard.Client.Dashboard');
    }
}
