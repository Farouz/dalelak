<?php

namespace App\Http\Controllers;

use App\Company;
use App\Menu;
use foo\bar;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class MenusController extends Controller
{
    //Add Menu Page
    public function getAddMenu($id)
    {
        $company = Company::find($id);
        return view('Dashboard.Client.Menu.AddMenu', compact('company'));
    }

    //Add Menu Function
    public function postAddMenu(Request $request)
    {
        $company_id = $request['company_id'];
        $company = Company::find($company_id);
        if ($company->user_id == Auth::id()) {
            $this->validate($request, ['title' => 'required', 'avatar' => 'required|image']);
            $menu = new Menu();
            $menu->company_id = $request['company_id'];
            $menu->title = $request['title'];
            if ($request->hasFile('avatar')) {
                $menu_avatar = md5($request['avatar']->getClientOriginalName()) . '.' . $request['avatar']->getClientOriginalExtension();
                $request['avatar']->move(public_path('assets/images'), $menu_avatar);
                $menu->avatar = $menu_avatar;
            }
            $menu->save();
            return redirect()->route('GET_MENUS', $company_id)->with('success', 'تم اضافه المنيو');
        } else {
            return back();
        }

    }

    //Menus page
    public function getShowMenu($id)
    {
        $company = Company::find($id);
        if ($company->user_id == Auth::id()) {
            $menus = Menu::all();
            return view('Dashboard.Client.Menu.ShowMenus', compact('company', 'menus'));
        } else {
            return back();
        }

    }

    //Menu Edit Page
    public function getEditMenu($id)
    {
        $menu = Menu::find($id);
        $company = Company::where('id', $menu->company_id)->first();
        if ($company->user_id == Auth::id()) {
            return view('Dashboard.Client.Menu.EditMenu', compact('menu'));

        } else {
            return back();
        }
    }

    //Menu Edit Function
    public function postEditMenu(Request $request, $id)
    {

        $this->validate($request, ['avatar' => 'image']);
        $menu = Menu::find($id);
        $company_id = Company::where('id', $menu->company_id)->first()->id;
        $menu->title = $request['title'];
        if ($request->hasFile('avatar')) {
            $menu_avatar = md5($request['avatar']->getClientOriginalName()) . '.' . $request['avatar']->getClientOriginalExtension();
            $request['avatar']->move(public_path('assets/images'), $menu_avatar);
            $menu->avatar = $menu_avatar;
        }
        $menu->update();
        return redirect()->route('GET_MENUS', $company_id)->with('success', 'تم تعديل البيانات');
    }

    //Delete Menu
    public function deleteMenu($id)
    {
        $menu = Menu::find($id);
        $company = Company::where('id', $menu->company_id)->first();
        if ($company->user_id == Auth::id()) {
            Menu::destroy($id);
            return back()->with('success', 'تم حذف المنيو');

        } else {
            return back();
        }
    }

    //get ALL Sub menus belongs to certain menu
    public function getAllSubMenus($id){
        $menu=Menu::find($id);

        return view('Dashboard.Client.Menu.SubMenus',compact('menu'));
    }

}

