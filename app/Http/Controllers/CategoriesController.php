<?php

namespace App\Http\Controllers;

use App\Category;
use Illuminate\Http\Request;

class CategoriesController extends Controller
{
    //All Categories Page
    public function getAllCat()
    {
        $cats = Category::all();
        return view('Dashboard.Category.AllCats', compact('cats'));
    }

    //Add Category Page
    public function getAddCat()
    {
        return view('Dashboard.Category.AddCat');
    }

    //Add Category Function
    public function postAddCat(Request $request)
    {
        $this->validate($request, ['name_en' => 'required',
            'name_ar' => 'required',
            'avatar'=>'required|image'

        ]);
        $category = new Category();
        $category->name_en = $request['name_en'];
        $category->name_ar = $request['name_ar'];
        if ($request->hasFile('avatar')){
            $cat_avatar=md5($request['avatar']->getClientOriginalName()).'.'.$request['avatar']->getClientOriginalExtension();
            $request['avatar']->move(public_path('assets.images'),$cat_avatar);
            $category->avatar=$cat_avatar;
        }
        $category->save();
        return redirect()->route('GET_ALL_CATEGORIES')->with('success', 'تم اضافه القسم');
    }

    //Edit page
    public function getEditCat($id){
        $cat=Category::findOrFail($id);
        return view('Dashboard.Category.EditCat',compact('cat'));
    }

    //Edit Function
    public function postEditCat($id ,Request $request){
        $this->validate($request,['avatar'=>'image']);

        $cat=Category::find($id);
        $cat->name_en=$request['name_en'];
        if ($request->hasFile('avatar')){
            $cat_avatar=md5($request['avatar']->getClientOriginalName()).'.'.$request['avatar']->getClientOriginalExtension();
            $request['avatar']->move(public_path('assets.images'),$cat_avatar);
            $cat->avatar=$cat_avatar;
        }
        $cat->name_ar=$request['name_ar'];
        $cat->update();
        return redirect()->route('GET_ALL_CATEGORIES')->with('success','تم تعديل البيانات ');
    }
    public function deleteCat($id){
        Category::destroy($id);
        return back()->with('success','تم مسح القسم');
    }

}
