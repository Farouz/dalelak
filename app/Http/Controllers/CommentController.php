<?php

namespace App\Http\Controllers;

use App\Company;
use Illuminate\Http\Request;

class CommentController extends Controller
{
    //show comments in admin dashboard
    public function getComments($id)
    {
        $company = Company::find($id);
        return view('Dashboard.Clients.Comments', compact('company'));
    }

    //Show Comments in Client
    public function getMyComments($id)
    {
        $company = Company::find($id);
        return view('Dashboard.Client.Comments', compact('company'));
    }
}
