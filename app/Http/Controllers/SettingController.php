<?php

namespace App\Http\Controllers;

use App\Setting;
use Illuminate\Http\Request;

class SettingController extends Controller
{
    //View The Settings
    public function getSettings()
    {
        $settings = Setting::all();
        return view('Dashboard.Setting.settings', compact('settings'));
    }

    //Add Settings Page
    public function getAddSettings()
    {
        return view('Dashboard.Setting.AddSetting');
    }

    //Add Setting Function
    public function postAddSettings(Request $request)
    {
        $this->validate($request, ['site_title' => 'required', 'IOS_link' => 'required', 'Android_link' => 'required', 'contact_facebook' => 'required', 'logo' => 'required|image', 'background_image' => 'required|image']);
        $setting = new Setting();
        $setting->site_title = $request['site_title'];
        $setting->IOS_link = $request['IOS_link'];
        $setting->Android_link = $request['Android_link'];
        $setting->contact_facebook = $request['contact_facebook'];
        if ($request->hasFile('logo')) {
            $logo_image = md5($request['logo']->getClientOriginalName()) . '.' . $request['logo']->getClientOriginalExtension();
            $request['logo']->move(public_path('assets/images'), $logo_image);
            $setting->logo = $logo_image;
        }
        if ($request->hasFile('background_image')) {
            $background = md5($request['background_image']->getClientOriginalName()) . '.' . $request['background_image']->getClientOriginalExtension();
            $request['background_image']->move(public_path('assets/images'), $background);
            $setting->background_image = $background;
        }
        $setting->save();
        return redirect()->route('GET_SETTINGS');
    }

    //Edit Settings Page
    public function getEditSettings($id)
    {
        $setting = Setting::findOrFail($id);
        return view('Dashboard.Setting.EditSetting', compact('setting'));
    }

    //Edit Function
    public function postEditSettings(Request $request, $id)
    {
        $this->validate($request, ['logo' => 'image', 'background_image' => 'image']);
        $setting = Setting::find($id);
        $setting->site_title = $request['site_title'];
        $setting->IOS_link = $request['IOS_link'];
        $setting->Android_link = $request['Android_link'];
        $setting->contact_facebook = $request['contact_facebook'];
        if ($request->hasFile('logo')) {
            $logo_image = md5($request['logo']->getClientOriginalName()) . '.' . $request['logo']->getClientOriginalExtension();
            $request['logo']->move(public_path('assets/images'), $logo_image);
            $setting->logo = $logo_image;
        }
        if ($request->hasFile('background_image')) {
            $background = md5($request['background_image']->getClientOriginalName()) . '.' . $request['background_image']->getClientOriginalExtension();
            $request['background_image']->move(public_path('assets/images'), $background);
            $setting->background_image = $background;
        }

        $setting->update();
        return redirect()->route('GET_SETTINGS')->with('success', 'تم التعديل');

    }
}
