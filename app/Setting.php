<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Setting extends Model
{
    protected $table = 'settings';
    protected $fillable = ['background_image', 'logo', 'IOS_link', 'Android_link', 'contact_facebook'];
}
