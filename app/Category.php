<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected $fillable = ['name_en', 'name_ar','avatar'];
    protected $table = 'categories';
    public function Companies(){
        return $this->hasMany(Company::class ,'cat_id');
    }
}
