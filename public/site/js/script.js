
jQuery(window).load(function () {

    // PAGE LOADER

    $('.pre-load').stop().animate({opacity:0}, 500, function(){$('.pre-load').css({'display':'none'});});
    $('body').css({'overflow-y':'auto'});


    // ANIMATION
    Animate_box();
    $(document).scroll(function (){
        Animate_box();
    });

    function Animate_box() {
        var scroll_var = $(this).scrollTop();

        $('.animate-box').each(function (){
            var val_one = $(this).offset().top - $(window).height() + 80;
s
            if (scroll_var > val_one){
                if($(this).hasClass('left-in')) {
                    $(this).addClass('animated fadeInLeft');
                }else if($(this).hasClass('right-in')) {
                    $(this).addClass('animated fadeInRight');
                }else {
                    $(this).addClass('animated fadeInUp');
                }
            }
        });
    }

});
// short for $(document).ready()
$(function() {
    var timer;

    $(window).resize(function() {
        clearTimeout(timer);
        timer = setTimeout(function() {
            $('.tqnih-bg-body-home, .tqniah-bg-color-home').css("min-height", $(window).height() + "px" );
        }, 40);
    }).resize();
});

var a = 0;
$(window).scroll(function() {

  var oTop = $('#tqinah-counter').offset().top - window.innerHeight;
  if (a == 0 && $(window).scrollTop() > oTop) {
    $('.counter-value').each(function() {
      var $this = $(this),
        countTo = $this.attr('data-count');
      $({
        countNum: $this.text()
      }).animate({
          countNum: countTo
        },

        {

          duration: 10000,
          easing: 'swing',
          step: function() {
            $this.text(Math.floor(this.countNum));
          },
          complete: function() {
            $this.text(this.countNum);
            //alert('finished');
          }

        });
    });
    a = 1;
  }

});

$(document).ready(function() {

  $('.loop1').owlCarousel({
	center: true,
	items:3,
	loop:true,
	margin:0,
	autoplay:true,
	dots:true,
	navs: false,
	autoplayTimeout:8000,
	  responsive:{
		0:{
			items:1
		},
		600:{
			items:3
		},
		992:{
			items:3
		}
	}
});

});
$(document).ready(function() {

		  $('.loop2').owlCarousel({
			center: true,
			items:1,
			loop:false,
			margin:0,
			autoplay:true,
			navText: ['<i class="fa fa-angle-right"></i>', '<i class="fa fa-angle-left"></i>'],
			autoplayTimeout:8000,
			  responsive:{
				0:{
					items:1
				},
				600:{
					items:1
				},
				992:{
					items:1
				}
			}
		});

		});
$(document).ready(function() {

  $('.loop3').owlCarousel({
	center: true,
	items:1,
	loop:false,
	margin:0,
	autoplay:true,
	navText: ['<i class="fa fa-angle-right"></i>', '<i class="fa fa-angle-left"></i>'],
	autoplayTimeout:8000,
	  responsive:{
		0:{
			items:1
		},
		600:{
			items:1
		},
		992:{
			items:1
		}
	}
});

});	
$(document).ready(function() {

  $('.loop4').owlCarousel({
	center: true,
	items:1,
	loop:false,
	margin:0,
	autoplay:true,
	navText: ['<i class="fa fa-angle-right"></i>', '<i class="fa fa-angle-left"></i>'],
	autoplayTimeout:8000,
	  responsive:{
		0:{
			items:1
		},
		600:{
			items:1
		},
		992:{
			items:1
		}
	}
});

});
$(document).ready(function() {

	
    // WINDOW HEIGHT

    SliderHeight();
    $(window).resize(function (){
        SliderHeight();
    });

    function SliderHeight() {
        $('.win-height').css({'height': $(window).height()});
    }



    // SMOOTH SCROLL

    $('.smooth-a').click(function(){
        $('html, body').animate({
            scrollTop: $( $(this).attr('href') ).offset().top
        }, 1000);
        return false;
    });


    // OWL

    var owl_pro = $("#owl-demo-products");
    owl_pro.owlCarousel({
        itemsCustom : [
            [0, 1],
            [480, 2],
            [768, 3],
            [992, 4],
            [1200, 5]
        ],
        navigation : false
    });
    $(".next-pro").click(function(){
        owl_pro.trigger('owl.next');
    });
    $(".prev-pro").click(function(){
        owl_pro.trigger('owl.prev');
    });



    // TOOLTIP

    $('[data-toggle="tooltip"]').tooltip();

 
});
 // NAVBAR SMOOTH SCROLL

$('.navbar a,  .down-load a').click(function(){
    $('html, body').animate({
        scrollTop: $( $(this).attr('href') ).offset().top - $('.navbar').height() + 5
    }, 1000);
    return false;
	
});

// NAVBAR CUSTOM BY TQNIAH FOR WEB
$(document).ready(function() {
	$('.nav-tqniah button').on('click', function(){
		  var tqniah = $('.nav-tqniah .nav-tqniah-list-item');
		  if(tqniah.css('right') === '-400px') {
			tqniah.css({
			   "transition-property":"right",
			   "right":"0",
			});
			   $('body').css({
				  "margin-right":"320px",
			  });
			  $("body").append("<div class='tqniah-overlay-hidden'></div>"),
			  $('.tqniah-overlay-hidden').click(function(){
				  $('.nav-tqniah .nav-tqniah-list-item').css("right","-400px"),
				  $('body').css("margin-right","0"),
				  $('body').css("margin-right","0"),
				  $('.tqniah-overlay-hidden').remove();
			  });
		  }
		  else{
			 tqniah.css({
				"right":"-400px",
			});
			  $('body').css({
				  "margin-left":"0",
			  });
			  $('.tqniah-overlay-hidden').remove();
   				 return false;
			  
		  }
		});
});
$(document).ready(function() {
    new WOW().init(); 
});

