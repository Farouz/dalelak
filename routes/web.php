<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


//Logins functions
Route::get('admin-login', 'AdminsController@getDashboardLogin')->name('ADMIN_LOGIN');
Route::post('/dashb-login', 'AdminsController@postDashboardLogin')->name('POST_DASHBOARD_LOGIN');


//Dashboard Routes
Route::group(['prefix' => 'admin', 'middleware' => 'AuthAdmin:webadmin'], function () {
    //Dashboard Home
    Route::get('/', 'AdminsController@getDashboard')->name('DASHBOARD');

    //ADMINS CRUD
    Route::get('/logout', 'AdminsController@getlogout')->name('GET_ADMIN_LOGOUT');
    Route::get('/allAdmins', 'AdminsController@getAllAdmins')->name('get_all_admins');
    Route::get('/delete-admin/{id}', 'AdminsController@deleteAdmin')->name('DELETE_ADMIN');
    Route::get('/edit-admin/{id}', 'AdminsController@editAdmin')->name('EDIT_ADMIN');
    Route::post('/edit-admin/{id}', 'AdminsController@postEditAdmin')->name('POST_EDIT_ADMIN');
    Route::get('/add-admin', 'AdminsController@getAddAdmin')->name('ADD_ADMIN');
    Route::post('/add-admin', 'AdminsController@postAddAdmin')->name('POST_ADD_ADMIN');

    //Make Admin
    Route::get('make-admin/{id}', 'AdminsController@makeAdmin')->name('MAKE_ADMIN');

    //Clients CRUD and Routes

    Route::get('/all-clients', 'UsersController@getAllClients')->name('GET_ALL_CLIENTS');
    Route::get('/add-client', 'UsersController@getAddClient')->name('GET_ADD_CLIENT');
    Route::post('/add-client', 'UsersController@postAddClient')->name('POST_ADD_CLIENT');
    Route::get('/edit-client/{id}', 'UsersController@getEditClient')->name('EDIT_CLIENT');
    Route::post('/edit-client/{id}', 'UsersController@postEditClient')->name('POST_EDIT_CLIENT');
    Route::get('/delete-client/{id}', 'UsersController@deleteClient')->name('DELETE_CLIENT');
    Route::get('/active-client/{id}', 'UsersController@ActiveClient')->name('ACTIVE_CLIENT');
    Route::get('/deActive-client/{id}', 'UsersController@DeActiveClient')->name('DE_ACTIVE_CLIENT');
    Route::get('/client-companies/{id}', 'UsersController@getClientsCompany')->name('CLIENT_COMPANY');

    Route::get('/client-sub/{id}', 'UsersController@getSubCompany')->name('SHOW_CLIENT_COMPANY');

    //Comments Route
    Route::get('getComments/{id}', 'CommentController@getComments')->name('SHOW_COMPANY_COMMENT');

    //CATEGORIES CRUD
    Route::get('/allCategories', 'CategoriesController@getAllCat')->name('GET_ALL_CATEGORIES');
    Route::get('/add-category', 'CategoriesController@getAddCat')->name('GET_ADD_CATEGORY');
    Route::post('/add-category', 'CategoriesController@postAddCat')->name('POST_ADD_CATEGORY');
    Route::get('/edit-category/{id}', 'CategoriesController@getEditCat')->name('EDIT_CAT');
    Route::post('/edit-category/{id}', 'CategoriesController@postEditCat')->name('POST_EDIT_CATEGORY');
    Route::get('/delete-category/{id}', 'CategoriesController@deleteCat')->name('DELETE_CAT');


    //Settings
    Route::get('/getSettings', 'SettingController@getSettings')->name('GET_SETTINGS');
    Route::get('/AddSettings', 'SettingController@getAddSettings')->name('GET_ADD_SETTING');
    Route::post('/AddSettings', 'SettingController@postAddSettings')->name('POST_ADD_SETTING');
    Route::get('/EditSettings/{id}', 'SettingController@getEditSettings')->name('EDIT_SETTINGS');
    Route::post('/EditSettings/{id}', 'SettingController@postEditSettings')->name('POST_EDIT_SETTING');
});

//Client Login page
Route::get('/client-login', 'ClientsController@getLogin')->name('CLIENT_LOGIN');
Route::post('/client-login', 'ClientsController@postLogin')->name('POST_CLIENT_LOGIN');

//CLIENTS ROUTES
Route::group(['middleware' => 'guest', 'prefix' => 'client'], function () {
    Route::get('/dashboard', 'ClientsController@getDashboard')->name('Client_Dashboard');
//COMPANY CRUD

    Route::get('/add-company/{id}', 'CompaniesController@getAddCompany')->name('GET_ADD_COMPANY');
    Route::post('/add-company', 'CompaniesController@postAddCompany')->name('POST_ADD_COMPANY');
    Route::get('/all-company/{id}', 'CompaniesController@getMyCompanies')->name('GET_MY_COMPANIES');
    Route::get('/edit-company/{id}', 'CompaniesController@getEditCompany')->name('EDIT_COMPANY');
    Route::post('/edit-company/{id}', 'CompaniesController@postEditCompany')->name('POST_EDIT_COMPANY');
    Route::get('/delete-company/{id}', 'CompaniesController@DeleteCompany')->name('DELETE_COMPANY');
    Route::get('/show-company/{id}', 'CompaniesController@getShowCompany')->name('SHOW_COMPANY');
//Comments

    Route::get('get-comments/{id}', 'CommentController@getMyComments')->name('GET_COMMENTS');

// MENU CRUD
    Route::get('/get-menu/{id}', 'MenusController@getAddMenu')->name('GET_ADD_MENU');
    Route::post('/add-menu', 'MenusController@postAddMenu')->name('POST_ADD_MENU');
    Route::get('/show-menu/{id}', 'MenusController@getShowMenu')->name('GET_MENUS');
    Route::get('/edit-menu/{id}', 'MenusController@getEditMenu')->name('EDIT_MENU');
    Route::post('/edit-menu/{id}', 'MenusController@postEditMenu')->name('POST_EDIT_MENU');
    Route::get('/delete-menu/{id}', 'MenusController@deleteMenu')->name('DELETE_MENU');
    Route::get('/show-sub-menu/{id}', 'MenusController@getAllSubMenus')->name('SHOW_SUB_MENU');

    //Sub Menus
    Route::get('/add-submenus/{id}', 'SubMenuController@getAddSubMenu')->name('GET_ADD_ITEMS');
    Route::post('/add-submenu', 'SubMenuController@postAddSubMenu')->name('POST_ADD_SUB_MENU');
    Route::get('/edit-submenu/{id}', 'SubMenuController@getEditSubMenu')->name('EDIT_SUB_MENU');
    Route::get('/delete-submenu/{id}', 'SubMenuController@deleteSubMenu')->name('DELETE_SUB_MENU');


});

//WebSite View Routes
Route::get('/', 'HomeController@getIndex')->name('HOME');
//Get All Sections and Categories
Route::get('/allSections', 'HomeController@getAllSections')->name('GET_ALL_SECTIONS');
//Get All Sub Sections
Route::get('/subSections/{id}', 'HomeController@getSubSections')->name('GET_IN_SECTION');
//Get Single Sub
Route::get('/section/{id}', 'HomeController@getSingleSub')->name('GET_SUB_DETAILS');

//Send Comments
Route::post('/send-comment/{id}', 'HomeController@postSentComment')->name('SEND_COMMENT');


//Search Function
Route::post('/search', 'SearchController@search')->name('SEARCH');
